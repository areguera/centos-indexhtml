#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Render jinja2 templates with CentOS Project configuration values.
# Copyright (C) 2021 Alain Reguera Delgado
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# 
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import glob
import yaml
from datetime import datetime
from jinja2 import Environment, FileSystemLoader

html_template_file = sys.argv[1]
output_file = sys.argv[2]

# Load the CentOS Project website configuration files.
configuration = {}
configuration_files = glob.glob("../../../Webistes/www.dev.centos.org/_data/centos/*.yml")
for configuration_file in configuration_files:
    configuration_name = os.path.basename(configuration_file).replace(".yml", "")
    configuration_data = open(configuration_file)
    configuration[configuration_name] = yaml.load(configuration_data, Loader=yaml.FullLoader)

# Set the locales available using the PO directory structure as reference. This
# information is necessary to create the index.html file that redirects the user
# to the appropriate page localization.
locales = []
locale_dirs = glob.glob("./PO/??-??.po")
for locale_dir in locale_dirs:
    locale = os.path.basename(locale_dir).replace(".po", "")
    locales.append(locale) 

# Set the current year used in footer copyright note.
year = datetime.now().strftime('%Y')

# Expand templates with configuration files available.
html_template_loader = FileSystemLoader('./')
html_template_loader_env = Environment(loader=html_template_loader)
html_template = html_template_loader_env.get_template(html_template_file)
html_template_rendered = html_template.render(centos=configuration, year=year, locales=locales)

html = open(output_file, "w")
html.write(html_template_rendered)