msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-20 18:51-0300\n"
"PO-Revision-Date: 2021-03-13 14:21-0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh-TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.3.2\n"

#: HTML/index.l18n.html%2Bhtml[lang]:2-1
msgid "en-US"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.head.title:4-5
msgctxt "HTML/index.l18n.html+html.head.title:4-5"
msgid "Welcome to CentOS Stream 9"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.header.div.a.img[alt]:14-69
msgid "CentOS Project"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.header.h1:15-7
msgctxt "HTML/index.l18n.html+html.body.header.h1:15-7"
msgid "Welcome to CentOS Stream 9"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.header.p:16-7
msgid "The latest major release of the CentOS Stream distribution."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:29-7
msgid "About CentOS Stream"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h3:31-7
msgid "CentOS Stream is Continuous"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:33-7
msgid ""
"CentOS Stream is a continuous-delivery distribution providing each point-"
"release of Red Hat Enterprise Linux (RHEL). Before a package is formally "
"introduced to CentOS Stream, it undergoes a battery of tests and checks—both "
"automated and manual—to ensure it meets the stringent standards for "
"inclusion in RHEL. Updates posted to Stream are identical to those posted to "
"the unreleased minor version of RHEL. The aim? For CentOS Stream to be as "
"fundamentally stable as RHEL itself."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:35-7
msgid ""
"To achieve this stability, each major release of Stream starts from a stable "
"release of Fedora Linux—In CentOS Stream 9, this begins with Fedora 34, "
"which is the same code base from which RHEL 9 is built. As updated packages "
"pass testing and meet standards for stability, they are pushed into CentOS "
"Stream as well as the nightly build of RHEL. What CentOS Stream looks like "
"now is what RHEL will look like in the near future."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.figure.img[alt]:38-7
msgid "How Fedora becomes CentOS; how CentOS becomes RHEL"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h3:41-7
msgid "CentOS Stream is Community"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:43-7
msgid ""
"CentOS Stream is developed through collaboration between the CentOS "
"community and the RHEL engineering team. Although many CentOS Stream "
"contributions derive from Red Hat employees, CentOS Stream thrives on "
"community support. CentOS Stream is a stable, reliable platform for open "
"source communities to expand upon, allowing people from all areas and "
"backgrounds to collaborate in an open environment."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:45-7
msgid ""
"Because CentOS Stream ultimately becomes RHEL, contributors also have an "
"opportunity for their work to influence future builds of RHEL; this makes "
"CentOS Stream an ideal environment for creativity and forward-thinking."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:47-7
msgid "Getting CentOS Stream"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:49-7
msgid ""
"CentOS Stream can be downloaded as <a href=\"https://www.centos.org/centos-"
"stream/\">an ISO</a> from our mirrors and is compatible with 64-bit x86 "
"(x86_64 v2+), 64-bit ARM (AArch64), IBM Z (s390x Z14+), and IBM POWER "
"(ppc64le POWER9+) architectures."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:51-7
msgid "Contribute to CentOS Stream"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:53-7
msgid ""
"Community is at the heart of the CentOS Project, and there are many ways you "
"can contribute. A list of areas where you can contribute is available <a "
"href=\"https://wiki.centos.org/Contribute\">on the CentOS Wiki</a>."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:55-7
msgid ""
"Because CentOS Stream is upstream of RHEL, it offers an ideal environment "
"for applications which are designed be deployed in RHEL. We welcome and "
"encourage contributors from all backgrounds—especially those developing for "
"the post-RHEL production stream—to use CentOS Stream to build, test, and "
"deploy the applications that are special to you and to the greater Linux "
"community."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:57-7
msgid ""
"You can also contribute by joining (or creating) a Special Interest Group "
"(SIG) in an area of your interest. Visit the <a href=\"https://wiki.centos."
"org/SpecialInterestGroup/\">CentOS Wiki</a> to learn more."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:59-7
msgid "Learn More"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:61-7
msgid ""
"To learn more about CentOS Stream 9, visit the <a href=\"https://www.centos."
"org/stream9\">CentOS Website</a>."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.h6:78-11
msgid "About"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:81-13
msgid "About CentOS"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:83-13
msgid "Frequently Asked Questions (FAQs)"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:85-13
msgid "Special Interest Groups (SIGs)"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:87-13
msgid "CentOS Variants"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:89-13
msgid "Governance"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:91-13
msgid "Code of Conduct"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.h6:96-11
msgid "Community"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:99-13
msgid "Contribute"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:101-13
msgid "Forums"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:103-13
msgid "Mailing Lists"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:105-13
msgid "IRC"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:107-13
msgid "Calendar &amp; IRC Meeting List"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:109-13
msgid "Planet"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:111-13
msgid "Submit Bug"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.h2:116-11
msgid "The CentOS Project"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.p:117-11
msgid ""
"Community-driven free software effort focused on delivering a robust open "
"source ecosystem around a Linux platform."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.p:137-11
msgid ""
"Copyright © 2021 The CentOS Project | <a href=\"https://www.centos.org/legal"
"\">Legal</a> | <a href=\"https://www.centos.org/legal/privacy\">Privacy</a> "
"| <a href=\"https://git.centos.org/centos/centos.org\">Site source</a>"
msgstr ""
